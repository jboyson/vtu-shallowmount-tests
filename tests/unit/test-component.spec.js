import { shallowMount, mount } from "@vue/test-utils";
import { GlModal } from "@gitlab/ui";
import TestComponent from "@/components/TestComponent.vue";

describe("TestComponent", () => {
  it("render mount", () => {
    const wrapper = mount(TestComponent);
    expect(wrapper.html()).toMatchInlineSnapshot(`""`);
  });
  it("render mount:stubs:component", () => {
    const wrapper = mount(TestComponent, { stubs: { GlModal } });
    expect(wrapper.html()).toMatchInlineSnapshot(`""`);
  });
  it("render mount:stubs:string", () => {
    const wrapper = mount(TestComponent, { stubs: ["gl-modal"] });
    expect(wrapper.html()).toMatchInlineSnapshot(`
      <gl-modal-stub modalid="basic-modal-id" titletag="h4" title="Example title" modalclass="" size="md" dismisslabel="Close">
        <div>
          <h1>Child Component</h1>
          <p>Some test content</p>
        </div>
      </gl-modal-stub>
    `);
  });
  it("render shallowMount", () => {
    const wrapper = shallowMount(TestComponent);
    expect(wrapper.html()).toMatchInlineSnapshot(`
      <gl-modal-stub modalid="basic-modal-id" titletag="h4" title="Example title" modalclass="" size="md" dismisslabel="Close">
        <child-component-stub></child-component-stub>
      </gl-modal-stub>
    `);
  });
  it("render shallowMount:stubs:component", () => {
    const wrapper = shallowMount(TestComponent, { stubs: { GlModal } });
    expect(wrapper.html()).toMatchInlineSnapshot(`
      <b-modal-stub id="basic-modal-id" size="md" ignoreenforcefocusselector="" title="" titletag="h4" modalclass="gl-modal," headerclosecontent="&amp;times;" headercloselabel="Close" canceltitle="Cancel" oktitle="OK" cancelvariant="secondary" okvariant="primary" lazy="true">
        <child-component-stub></child-component-stub> <template></template> <template></template> <template></template>
      </b-modal-stub>
    `);
  });
  it("render shallowMount:stubs:string", () => {
    const wrapper = shallowMount(TestComponent, { stubs: ["gl-modal"] });
    expect(wrapper.html()).toMatchInlineSnapshot(`
      <gl-modal-stub modalid="basic-modal-id" titletag="h4" title="Example title" modalclass="" size="md" dismisslabel="Close">
        <child-component-stub></child-component-stub>
      </gl-modal-stub>
    `);
  });
});
